package bin.littleprolog.ast;

import java.lang.Runnable;

public class ActionWriteOutput implements Runnable {

    private String msg, prefix;

    public ActionWriteOutput(String m, int level) {
        msg = m;
        prefix = "|---";
        for(int i=0; i<level; i++) {
            prefix = "   " + prefix;
        }
    }

    public void run() {
        System.out.println(prefix + " " + msg);
    }
}
