package bin.littleprolog.common;

import java.util.HashMap;
import java.util.Stack;
import java.util.ArrayList;
import java.lang.Integer;
import java.util.List;

public class Tokeniser {

    private HashMap<String, Integer> WEIGHT = new HashMap<String, Integer>();

    public Tokeniser() {
        WEIGHT.put("=", 100);
        WEIGHT.put("+", 200);
        WEIGHT.put("-", 200);
        WEIGHT.put("*", 400);
        WEIGHT.put("/", 400);
        WEIGHT.put("==", 700);
        WEIGHT.put(">", 700);
        WEIGHT.put(">=", 700);
        WEIGHT.put("<", 700);
        WEIGHT.put("<=", 700);
        WEIGHT.put(":-", 1200);
    }

    /**
     * This method is a wrapper for the tokeniser and the makeStack methods.
     * @param prolog a String containing the Prolog source code
     * @return  returns a stack of tokens
     */
    public ArrayList<Token> process(String prolog) {
        ArrayList<Token> tokens = new ArrayList<Token>();
        try {
            NParam counter = new NParam(null);
            tokens = tokenise(prolog, 0, 0, tokens, counter);
        } catch(SyntaxError e) {
            System.out.println(e);
            tokens.clear();
        }
        return tokens;
    }

    private ArrayList<Token> tokenise(String line, int i, int tnest, ArrayList<Token> tokens, NParam counter) throws SyntaxError {
        Character c;
        String tclass, tvalue;
        int j, oplen;
        if(i < line.length()) {
            i = skipWhitespace(line, i);
            i = skipRemark(line, i);

            // Test for special case single character occurence
            c = line.charAt(i);
            switch(c) {
                case '.':
                    tokens.add(new Token("STOP", ".", tnest));
                    return tokenise(line, i+1, tnest, tokens, counter);
                case ',':
                    if(tnest == 0) {
                        tokens.add(new Token("NEXT", ",", 0));
                    }
                    return tokenise(line, i+1, tnest, tokens, counter);
                case ')':
                    return tokenise(line, i+1, tnest-1, tokens, counter.previous());
            }
            // Determine Token Class
            j = i;
            if(Character.isDigit(c)) {
                tclass = "INTEGER";
                while(j < line.length()) {
                    c = line.charAt(j);
                    if(Character.isDigit(c)) {
                        j++;
                    } else {
                        break;
                    }
                }
                tvalue = substring(line, i, j);
                tokens.add(new Token(tclass, tvalue, tnest));
                if(tnest > 0) { counter.add(); }
                return tokenise(line, j, tnest, tokens, counter);
            } else if(Character.isUpperCase(c) || c == '_') {
                tclass = "VARIABLE";
                while(j < line.length()) {
                    c = line.charAt(j);
                    if(Character.isLetterOrDigit(c) || c == '_') {
                        j++;
                    } else {
                        break;
                    }
                }
                tvalue = substring(line, i, j);
                tokens.add(new Token(tclass, tvalue, tnest));
                if(tnest > 0) { counter.add(); }
                return tokenise(line, j, tnest, tokens, counter);
            } else if(Character.isLowerCase(c)) {
                tclass = "ATOM";
                while(j < line.length()) {
                    c = line.charAt(j);
                    if(Character.isLetterOrDigit(c)) {
                        j++;
                    } else {
                        break;
                    }
                }
                tvalue = substring(line, i, j);
                j = skipWhitespace(line, j);
                if(line.charAt(j) == '(') { tclass = "PREDICATE"; }
                tokens.add(new Token(tclass, tvalue, tnest));
                if(tnest > 0) { counter.add(); }
                if(line.charAt(j) == '(') {
                    tnest++;
                    counter = new NParam(counter);
                    tokens.add((Token) counter);
                    j++;
                }
                return tokenise(line, j, tnest, tokens, counter);
            } else if(c == '\'') {
                tclass = "STRING";
                i++;
                j++;
                while(j < line.length()) {
                    c = line.charAt(j);
                    if(c == '\'') {
                        tvalue = substring(line, i, j);
                        tokens.add(new Token(tclass, tvalue, tnest));
                        counter.add();
                        return tokenise(line, j+1, tnest, tokens, counter);
                    } else {
                        j++;
                    }
                }
                throw new SyntaxError("String not enclosed");
            } else {
                oplen = 0;
                if(WEIGHT.containsKey(substring(line, i, i+2))) {
                    oplen = 2;
                } else if(WEIGHT.containsKey(substring(line, i, i+1))) {
                    oplen = 1;
                }
                if(oplen > 0) {
                    j = i + oplen;
                    tclass = "OPERATOR";
                    tvalue = substring(line, i, j);
                    tokens.add(new Token(tclass, tvalue, tnest));
                    return tokenise(line, j, tnest, tokens, counter);
                }
                return tokens;
            }
        } else {
            return tokens;
        }
    }

    private int skipRemark(String line, int i) {
        if(substring(line, i, i+2).equals("/*")) {
            while(i+2 <= line.length()) {
                if(substring(line, i, i+2).equals("*/")) {
                    i += 2;
                    break;
                }
                i += 1;
            }
        }
        return i;
    }

    private int skipWhitespace(String line, int j) {
        Character c = line.charAt(j);
        while(j < line.length()) {
            c = line.charAt(j);
            if(Character.isWhitespace(c)) {
                j++;
            } else {
                break;
            }
        }
        return j;
    }

    public Stack<Token> toRPN(List<Token> tokens) {
        boolean rpnmode = false;
        Stack<Token> newstack = new Stack<Token>();
        Stack<Token> rpnstack = new Stack<Token>();
        Stack<Token> opstack = new Stack<Token>();
        if(tokens.size() > 0) {
            rpnmode = !tokens.get(0).isPredicate();
            int i = 0;
            while(i < tokens.size()) {
                if(rpnmode) {
                    rpnpush(tokens.get(i), rpnstack, opstack);
                } else {
                    newstack.add(tokens.get(tokens.size() -i - 1));
                }
                i++;
            }
            if(rpnmode) {
                while(opstack.size() > 0) { rpnstack.add(opstack.pop()); }
                return rpnstack;
            }
            return newstack;
        }
        return newstack;
    }


    private void rpnpush(Token token, Stack<Token> rpnstack, Stack<Token> opstack) {
        if (token.isOperator()) {
            if (higher(token, opstack)) {
                opstack.add(token);
            } else {
                rpnstack.add(opstack.pop());
                opstack.add(token);
            }
        } else {
            rpnstack.add(token);
        }
    }

    private boolean higher(Token token, Stack<Token> opstack) {
        Token t2;
        if (opstack.size() == 0) {
            return true;
        } else {
            return WEIGHT.get(token.getTValue()) > WEIGHT.get(opstack.peek().getTValue());
        }
    }

    private String substring(String string1, int start, int end) {
        if(start < 0) { start = 0; }
        if(end > string1.length()) { end = string1.length(); }
        return string1.substring(start, end);
    }


}
