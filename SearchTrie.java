package bin.littleprolog.database;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import bin.littleprolog.common.Token;

/**
 * Implementation of Searrch Trie to store facts
 */

public class SearchTrie implements Serializable {

    TrieNode root;

    /**
     * Default constructor
     * @param t the token representing the root of the Search Trie. It is actually the token that corresponds to the predicate at level 0.
     */
    public SearchTrie(Token t) {
        root = new TrieNode(t);
    }

    /**
     * A fact is represented as a series of tokens. The tokens are inserted into the Search Trie.
     * This is actually a depth-first traversal of the Search Trie.
     * @param tokens the fact
     */
    public void insert(ArrayList<Token> tokens) {
        int i = 0;
        TrieNode nCurrent = root;
        Token tCurrent, tNext;
        if(tokens.size() > 1 && root.match(tokens.get(0))) {
            while(true) {
                tCurrent = tokens.get(i);
                tNext = tokens.get(i+1);
                if(tNext.isNParam()) {
                    i += 1;
                    tNext = tokens.get(i+1);
                }
                if(nCurrent.hasChild(tNext)) {
                    nCurrent = nCurrent.getChild(tNext);
                } else {
                    nCurrent.addChild(tNext);
                    nCurrent = nCurrent.getChild(tNext);
                }
                if( i == tokens.size() - 2) {
                    nCurrent.setTerminal();
                    break;
                }
                i += 1;
            }
        }
    }

    /**
     * This is wrapper for the actual search algorithm
     * @param tokens the fact to be queried against the database
     * @return a Tracker object that stores the result of the search
     */
    public Tracker search(ArrayList<Token> tokens, HashMap<String, Token> variables) {
        Tracker tracker = new Tracker();
        traverse(tokens,tracker, null, 1, -1, variables);
        return tracker;
    }

    /**
     * This is the actual algorithm for predicate/variable search
     * @param tokens an ordered ArrayList of tokens representing a predicate/variable search
     * @param tracker the Tracker object for storing the result of the search
     * @param nParent the Parent of the Trie Node. Default value is null
     * @param i the index of the current token. Default value is 1
     * @param id The path id of the current search path. Default value is -1.
     */
    private void traverse(ArrayList<Token> tokens, Tracker tracker, TrieNode nParent, int i, int id, HashMap<String, Token> variables) {
        int newid;
        Token tCurrent;
        if(id == -1) { id = tracker.getId(-1); }
        if(tokens.size() > 1 && i <= tokens.size()) {
            if(i == tokens.size()) {
                if(nParent.isTerminal()) {
                    tracker.verify(id);
                }
            } else if (root.match(tokens.get(0))) {
                if(nParent == null) { nParent = root; }
                tCurrent = tokens.get(i);
                if(tCurrent.isVariable() ) {
                    if(variables.containsKey(tCurrent.getTValue())) {
                        int nest = tCurrent.getTNest();
                        Token matched = variables.get(tCurrent.getTValue()).getToken(nest);
                        if(nParent.hasChild(matched)) {
                            nParent = nParent.getChild(matched);
                            traverse(tokens, tracker, nParent, i+1, id, variables);
                        }
                    } else {
                        for(TrieNode child: nParent.getChildren()) {
                            newid = tracker.getId(id);
                            tracker.match(newid, tCurrent, child.token);
                            traverse(tokens, tracker, child, i+1, newid, variables);
                        }
                    }
                } else if(nParent.hasChild(tCurrent)) {
                    nParent = nParent.getChild(tCurrent);
                    traverse(tokens, tracker, nParent, i+1, id, variables);
                }
            }
        }
    }
}
