package bin.littleprolog.ast;

import java.util.HashMap;
import bin.littleprolog.common.Token;

public class ActionAssign implements Runnable {
    private String varname;
    private Token varvalue;
    private String prefix = "|---";
    private HashMap<String,Token> variables;

    public ActionAssign(String n, Token t, HashMap<String, Token> v, int level) {
        varname = n;
        varvalue = t;
        variables = v;
        for(int i=0; i<level; i++) {
            prefix = "   " + prefix;
        }
    }

    public void run() {
        variables.put(varname, varvalue);
        System.out.println(prefix + " " + varname + " = " + varvalue.toString());
    }

}
