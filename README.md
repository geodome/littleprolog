# README #

LittleProlog is a Prolog Interpreter written in Java.


## WHAT IS THIS REPOSITORY FOR? ##

* This project originally started out as an academic exercise to explore the construction and design of compilers and interpreters
* The Prolog Language is chosen because it is a relatively small language.

## INSTRUCTIONS##

## How do I set up? ##

* The following instruction assumes a Linux terminal
* First set the directory where you want the compiled classes to be stored.
```
DIR="where/you/want/"
```
* Next, goto the directory where you had downloaded the Java source code. Then perform:
```
# First compile the bin.littleprolog.common package
javac -d $DIR Token.java NParam.java SyntaxError.java Tokeniser.java
# Then compile the bin.littleprolog.database package
javac -d $DIR Tuple.java Tracker.java TrieNode.java SearchTrie.java
# Follow by the bin.littleprolog.ast package  
javac -d $DIR ActionAssign.java ActionSetVariable.java ActionWriteOutput.java AST.java Generator.java
# Finally the main package bin.littleprolog itself.
javac -d $DIR LittleProlog.java PrologShell.java
```

## To run the Prolog Shell ##

```
cd $DIR
java bin.littleprolog.PrologShell
```

## How to use in your programming project? ##

* The Interpreter is to be instantiated as an Object.
* It only has 2 interface - onDatabase() and onQuery().
* onQuery() executes the Prolog code given and prints an output.
* onDatabase() adds Prolog facts to the Interpreter's database.

Import the littleprolog package
```Java
import bin.littleprolog.LittleProlog;
```

Instantiate the LittleProlog class
```Java
LittleProlog test = new LittleProlog(null);
```
* To send facts to the Prolog database

```java
String fact = "bigger(lion,cat).";
test.onDatabase(fact);
```

* Predicate search

```java
String query1 = "bigger(lion,cat).";
test.onQuery(query1);
```

* Variable search
```java
String query2 = "bigger(X, cat)."
test.onQuery(query2);

```
* Running multiple statements

```Java
String query3 = "1 + 2, write('hello')."
test.onQuery(query3);
```

## FEATURES ##

Finished features are marked with X. Unmarked features are work in progress

- [X] Shell Interface
- [X] I/O Built-in: write()
- [_] I/O Built-in: read(), consult()
- [X] Unification: Predicate and Variable Search
- [X] Primary Data Type: Atom, Integer, String, Variable
- [_] Assignment Operator: = (Requires variable solver)
- [_] Variable Solver for Variable Data Type (need to resolve data type at run-time)
- [_] Secondary Data Type: List[H|T]
- [_] List Operations: is_list(term), memberchk(element, list), length(list), sort(list)
- [X] Arithmetic Operation: +, -, \*, /,
- [X] Comparison Operation: >, >=, ==, <=, <
- [_] String operations
- [_] Logic Operators: Not(!), Or(;)
- [_] Clause Operator: :-
- [X] Local variables
- [_] Global variables


## KNOWN BUGS ##

* 99 little bugs in the code
* Take one down, patch it around
* 127 little bugs in the code

## DESIGN PHILOSOPHY ##

The program consists of 4 packages

* the main package `bin.littleprolog`
* the common package `bin.littleprolog.common`
* the database package `bin.littleprolog.database`
* the ast package `bin.littleprolog.ast`

## The Main Package ##

`bin.littleprolog` consists of PrologShell and LittleProlog. `LittleProlog` is the Prolog Interpreter
while `PrologShell` provides an interactive command-line interface to the Prolog Interpreter.

LittleProlog has 2 key interfaces - the `onQuery(String prolog)` and the `onDatabase(String prolog)` methods.
The string prolog in each method refers to the Prolog source code input by the user, whether it is a fact
to be entered into the Prolog Database or a query to be executed by the Interpreter.

The `bin.littleprolog.LittleProlog.onQuery()` method calls upon the Tokeniser in the `bin.littleprolog.common`
package to process the Prolog string input by the user. The generated token list may consist of several
sentences (ie..a sublist of tokens), each separated by a `NEXT` or `STOP` token.

Each sentence is passed to a `bin.littleprolog.ast.Generator` object. Each Generator object is stored
in an arraylist in the same order of the sentences. When onQuery detects the `STOP` token, it calls the
`invoke()` method to execute each Generator in the  arraylist. If the current Generator yields
a True outcome, then the next Generator is invoked, otherwise the `invoke()` method ceases.

The `onDatabase()` method passes Prolog facts to the database. The Prolog database is actually a hashmap,
whose key is the predicate name while the corresponding value is a Trie graph. If the predicate name doesn't
in the database, the onDatabase() method creates a new entry for the predicate. Otherwise, it inserts the
facts into the corresponding trie graph.

## The Common Package ##

The `bin.littleprolog.common` package consists of the `Token`, `NParam`, `Tokeniser` and `SyntaxError` classes.

## Tokeniser and SyntaxError ##

`Tokeniser.process()` reads the Prolog source code and breaks it down into a series of Tokens,
each corresponding  to a different construct of the Prolog Language.  The tokens form the fundemental
representation of the program as they dictate the workflow of the program.

One important aspect of the Tokeniser is that it reorders the tokens via the `Tokeniser.toRPN()` method.
RPN stands for Reverse Polish Notation. Tokens ordered in RPN can be used directly to construct an abstract
syntax tree. Note that the RPN order only matters for arithmetic, logical and comparison operations, so the
`Tokeniser.toRPN()` method actually retains the original order of the tokens that relate to predicate operations
such as search, read and other builtin functions.

Whenever the Tokeniser fails to recognise a symbol in the given Prolog string, it yields a `SyntaxError`
exception. The `SyntaxError` exception may be yielded by other parts of the program such as the
bin.littleprolog.ast package.

## Token & NParam ##

Each token is described by its token class  (tclass), its token value (tvalue) and its nesting level (tnest).

The default nesting level 0  indicates the token is not nested inside a predicate, while nesting level > 0
indicates the token is part of a predicate or an inner predicate. For example, for the following predicate
`hello1(world1, hello2(world2), world3)`, the nesting level for `hello1` is 0, while the nesting level for
`world1`, `hello2` and `world3` is 1 and the nesting level for `world2` is 2.

There are 9 token classes:

* `INTEGER`
* `STRING`
* `ATOM`
* `VARIABLE`
* `OPERATOR`
* `PREDICATE`
* `NPARAM`
* `NEXT`
* `STOP`

LittleProlog supports 3 primitive data types - `STRING`, `INTEGER` and `ATOM`. The tvalue of these classes
holds their string literal repesentation. For example, the integer 1 is represented as tclass
`INTEGER`, tvalue "1" while the string "abc" is represented as tclass `STRING`, tvalue "abc". The atom
lion is described as tclass `ATOM`, tvalue "lion".

The `VARIABLE` token class represents a Prolog Variable. The tvalue represents the name of the variable.
A Prolog variable may refer to null, an Integer, a String or an Atom. The references are  stored in the `
bin.littleprolog.LittleProlog.variables` hashmap. The key of the hashmap is the variable name while the
value corresponds to the token representation of the value it is supposed to store. For example, if
the variable X is to refer to the integer 3, then the value entry for X would be the `INTEGER` TOKEN 3. If
the variables hashmap doesn't contain the variable name, then the value for the variable is null.

The `OPERATOR` token class represents a Prolog operator. The tvalue of each operator includes symbols like
+, -, <, :- and  they represent well defined operations such as addition, subtraction, clause whereby the
arity (number of operands) and precedence (weight) are already known.

The `PREDICATE` token describes a functor and it is always followed by the `NPARAM` token. Unlike an operator,
the arity of a predicate is unknown until run-time, so the arity has to be counted at run-time. The
arity is stored as the tvalue of the `NPARAM` token. `NPARAM` stands for Number of PARAMeters. The tvalue of
the `PREDICATE` token is the name of the functor it represents.

The `NEXT` token refers to the `,` operator, while the STOP token refers to the `.` operator. Both are
singled out and not treated as an OPERATOR token because of their significance to run-time evaluation. A
Prolog code may consist of several sentences. Each sentence is separated by the `,` operator which is
denoted  as the `NEXT` token. The last sentence should be terminated with the `.` operator which is
denoted as the `STOP` token. The NEXT and STOP tokens tell `bin.littleprolog.LittleProlog.onQuery()`
which sublist of tokens forms a statement and when to invoke Generator objects for execution respectively.
This is particularly important because an abstract syntax tree can only be constructed from a sentence.

## The Database Package ##

The `bin.littleprolog.database` package consists of the `Tuple`, `Tracker`, `TrieNode` and `SearchTrie` classes.

Prolog facts are stored in a hashmap called `bin.littleprolog.LittleProlog.database`. The key of each entry is the predicate name
and the corresponding value is a [Trie](https://en.wikipedia.org/wiki/Trie) graph representing all the facts associated to that
predicate. The trie graph is implemented by the `SearchTrie` class and each node on the trie graph corresponds to a Token. Each
node is implemented by the `TrieNode` class.

The `bin.littleprolog.LittleProlog.onDatabase()` method invokes the `SearchTrie.insert()` method to insert a new fact while the
`bin.littleprolog.LittleProlog.onQuery()` invokes the `SearchTrie.search()` method to invoke a predicate or variable search. A predicate
or variable search is done by depth-first traversal of the Search Trie. Every time the traversal matches a `VARIABLE` token in the search
terms to a Trie Node, the match is stored as a `Tuple` in a `Tracker` object.


## The AST package ##

To be added later


## WHO DO I TALK TO? ##

* Repo owner or admin
* Other community or team contact
