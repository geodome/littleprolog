package bin.littleprolog.common;

import java.lang.Integer;

public class NParam extends Token {

    private NParam prev;
    private int count;

    public NParam(NParam previous) {
        super("NPARAM", "0", 0);
        count = 0;
        if(previous != null) { prev = previous; }
    }

    public NParam previous() {
        return prev;
    }

    public void add() {
        count += 1;
        setTValue(Integer.toString(count));
    }

    public int getCount() {
        return count;
    }
}
