package bin.littleprolog.common;

public class SyntaxError extends Exception {
    public SyntaxError(String msg) {
        super(msg);
    }
}
