package bin.littleprolog.database;

import java.io.Serializable;

/**
 * The Tuple class is for holding matched tokens, ie. a variable token and a non-variable token (string, atom, integer)
 * @param <E> a generic type, usually Token for this case
 */
public class Tuple<E> implements Serializable{

    private E left, right;

    /**
     * Default constructor
     * @param left left item
     * @param right right item
     */
    public Tuple(E left, E right) {this.left = left; this.right = right; }

    /**
     * String representation of the Tuple
     * @return
     */
    public String toString() {return left.toString() + " " + right.toString(); }

    /**
     * Return the left item
     * @return
     */
    public E getLeft() { return left; }

    /**
     * Return the right item
     * @return
     */
    public E getRight() { return right; }

    /**
     * Checks if both left and right items are null. This is important because an Empty Tuple
     * represents 'predicate found' for a predicate search
     * @return True or False
     */
    public boolean isEmpty() { return left == null && right == null; }

}
