import java.io.Console;

/**
 * This program provides the Shell Interface. To run it, run "java PrologSHell" in the terminal.
 */
public class PrologShell {

    /**
     * The main() method provides the Read-Eval-Print-Loop interface to act as a shell
     */
    public static void main(String[] args) {
        LittleProlog interpreter = new LittleProlog(null);
        Console console = System.console();
        String userinput, output;
        boolean exit = false;

        if(console == null) { System.exit(1); }

        // add some facts for testng
        interpreter.onDatabase("animal(lion).");
        interpreter.onDatabase("animal(tiger).");
        interpreter.onDatabase("animal(cat).");
        interpreter.onDatabase("bigger(lion, tiger).");
        interpreter.onDatabase("bigger(tiger, cat).");

        while(!exit) {
            userinput = console.readLine(">> ");
            if(userinput.equals("exit.")) {
                exit = true;
            } else {
                output = interpreter.onQuery(userinput);
                console.format(output);
            }
        }
        System.exit(0);
    }
}
