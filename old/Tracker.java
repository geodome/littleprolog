import java.util.ArrayList;
import java.util.Iterator;

/**
 * The Tracker class stores the result of the backtracking done by the TrieSearch search algorithm
 */
public class Tracker implements Iterator<ArrayList<Tuple<Token>>> {
    private int currentid = -1;
    private ArrayList<Integer> vtable = new ArrayList<Integer>();
    private ArrayList<ArrayList<Tuple<Token>>> table = new ArrayList<ArrayList<Tuple<Token>>>();
    int index = 0;

    /**
     * Default constructor
     */
    public Tracker() {
    }

    /**
     * Create a unique path id for each backtracking pathway. It is invoked when a variable token is found.
     * @param id -1 to indicate a new path, otherwise the id number of another path which the current path is to be derived from
     * @return the id number for the current path
     */
    public int getId(int id) {
        currentid += 1;
        if (id == -1) {
            table.add(new ArrayList<Tuple<Token>>());
        } else {
            ArrayList<Tuple<Token>> newlist = new ArrayList<Tuple<Token>>();
            for (Tuple<Token> t : table.get(id)) {
                newlist.add(t);
            }
            table.add(newlist);
        }
        return currentid;
    }

    /**
     * Informs the backtracker a variable-token match is found for the current path (identified by id)
     * @param id the path id
     * @param token1
     * @param token2
     */
    public void match(int id, Token token1, Token token2) {
        ArrayList<Tuple<Token>> solution = table.get(id);
        if (solution != null) {
            solution.add(new Tuple(token1, token2));
        }
    }

    /**
     * This method informs the backtracker that path id has reached the end of the Search Trie
     * @param id the path id of the current search path
     */
    public void verify(int id) {
        vtable.add(id);
        if (table.get(id).size() == 0) {
            table.get(id).add(new Tuple(null, null));
        }
    }

    /**
     * This method checks if any solution has been found
     * @return true or alse
     */
    public boolean noMatches() {
        return vtable.size() == 0;
    }

    /**
     * This method is part of the Iterator interface, allows the retrieval of Tracker result
     * @return true if there are results to be retrieved
     */
    @Override
    public boolean hasNext() {
        return index < vtable.size();
    }

    /**
     * This method is part of the Iterator interface, allows retrieval of the Tracker resu;t
     * @return an ArrayList of tuples indicating a search result
     */
    @Override
    public ArrayList<Tuple<Token>> next() {
        int id = vtable.get(index);
        index += 1;
        return table.get(id);
    }

    @Override
    public void remove() {}

}
