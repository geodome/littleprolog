import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * Created by geodome on 12/10/16.
 */
public class LittleProlog  {

    private HashMap<String, Integer> WEIGHT = new HashMap<String, Integer>();
    private HashMap<String, SearchTrie> database;
    private HashMap<String, Token> variables = new HashMap<String, Token>();
    /**
     * To signal syntax error in the Interpreter
     */
    public class SyntaxError extends Exception {
        public SyntaxError(String msg) {
            super(msg);
        }
    }


    /**
     * Node for the Abstract Syntax Tree
     */
    private class Node{

        public Token token;
        public ArrayList<Node> children = new ArrayList<Node>();
        public Queue<Runnable> queue;
        public ArrayList<String> output;

        /**
         * Default constructor
         * @param t Prolog token
         * @param q Action Queue
         * @param o ArrayList to store output
         */
        public Node(Token t, Queue<Runnable> q, ArrayList<String> o) {
            token = t;
            queue = q;
            output = o;
        }

        /**
         * Returns a string representation of a Token
         * @return string
         */
        public String toString() {
            return getTClass() + ", " + getTValue() + ", " + Integer.toString(getTNest());
        }

        /**
         * Retrieves the token
         * @return token
         */
        public Token getToken() {
            return token;
        }

        /**
         * returns the token class
         */
        public String getTClass() {
            return token.getTClass();
        }

        /**
         * returns the token value
         */
        public String getTValue() {
            return token.getTValue();
        }

        /**
         * return the token's nested level
         */
        public int getTNest() {
            return token.getTNest();
        }

        /**
         * This method adds a child to the node
         */
        public void addChild(Node child) {
            children.add(child);
        }

            /**
             * This method informs the number of children a node has
             */
            public int nChildren() {
                return children.size();
            }

            /**
             * This method retrieves a child node
             */
            public Node getChild(int i) {
                return children.get(i);
            }


            /**
             * This method generates the Token List
             */
            public ArrayList<Token> getTokenList() {
                System.out.println("children of " + token + " is " + children);
                ArrayList<Token> aggregate = new ArrayList<Token>();
                aggregate.add(token);
                for(int i=0; i < children.size(); i++) {
                    ArrayList<Token> tokens = getChild(i).getTokenList();
                    aggregate.addAll(tokens);
                }
                System.out.println("aggregated: " + aggregate);
                return aggregate;
            }


        }

        private class ActionAssign implements Runnable {
            private String varname;
            private Token varvalue;
            private ArrayList<String> output;

            public ActionAssign(String v, Token t, ArrayList<String> o) {
                varname = v;
                varvalue = t;
                output = o;
            }

            public void run() {
                variables.put(varname, varvalue);
                output.add(varname + " = " + varvalue.toString());
            }
        }
        /**
         * Every Prolog statement has 2 outcome - it either sets a Variable or prints rman output.
         * This class implements a Runnable which sets a set of variables.
         */
        private class ActionSetVariable implements Runnable {

            ArrayList<Tuple<Token>> resolutions;
            ArrayList<String> output;

            /**
             * Default constructor
             * @param r an ArrayList containing tuples which matches a variable to a Token (atom, string, integer)
             * @param o an ArrayList to store the output string
             */
            public ActionSetVariable(ArrayList<Tuple<Token>> r, ArrayList<String> o) {
                resolutions = r;
                output = o;
            }

            /**
             * This method invokes the runnable
             */
            public void run() {
                for (Tuple<Token> r : resolutions) {
                    if(r.isEmpty()) {
                        output.add("Predicate found\n");
                        break;
                    }
                    Token var = r.getLeft();
                    Token val = r.getRight();
                    variables.put(var.getTValue(), val);
                    output.add(var.getTValue() + " = " + val.getTValue() + "\n");
                }
            }
        }

        /**
         * Every Prolog statement has 2 outcome - it either sets a Variable or prints an output.
         * This class implements a Runnable which prints the output
         */
        private class ActionWriteOutput implements Runnable {
            private String message;
            private ArrayList<String> output;

            public ActionWriteOutput(String m, ArrayList<String> o) {
                output = o;
                message = m;
            }

            @Override
            public void run() {
                output.add(message + "\n");
            }
        }

        /**
         * This is a custom implementation of the Node Class for a Predicate
         */
        private class Predicate extends Node {

            /**
             * Constructor
             * @param t Token
             * @param q Action Queue
             * @param o ArrayList which stores the screen output
             */
            public Predicate(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            /**
             * This method is called upon when a Node is executed. At the moment, it only supports
             * the built-in write() function and predicate/variable search. Add
             */
            public void execute() {
                if (token.getTValue().equals("write")) {
                    String msg = ((pString) getChild(0)).execute();
                    queue.add(new ActionWriteOutput(msg, output));
                } else {
                    search();
                }
            }

            /**
             * In the Prolog database, each predicate is stored as a Search Trie.
             * To invoke search, one must access the search method of the Search Trie
             */
            public void search() {
                SearchTrie trie = (SearchTrie) database.get(token.toString());
                if (trie == null) {
                    queue.add(new ActionWriteOutput("No such fact", output));
                } else {
                    Tracker result;
                    ArrayList<Token> searchtokens = getTokenList();
                    for (result = trie.search(searchtokens, variables); result.hasNext(); ) {
                        ArrayList<Tuple<Token>> resolutions = result.next();
                        queue.add(new ActionSetVariable(resolutions, output));
                    }
                    if (result.noMatches()) {
                        queue.add(new ActionWriteOutput("No matches found", output));
                    }

                }
            }

        }

        /**
         *  This pAtom class implements an Atom as a AST Node
         */
        private class pAtom extends Node {

            /**
             * Default constructor
             * @param t the Atom Token
             * @param q the Action Queue
             * @param o the output string array
             */
            public pAtom(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            /**
             * This method is called upon when an atom is called
             * @return the name of the atom
             */
            public String execute() {
                return getTValue();
            }
        }

        /**
         * Parent Node class for Math operators (+, -, *, /).
         */
        private class MathOperator extends Node {

            /**
             * Default constructor
             * @param t
             * @param q
             * @param o
             */
            public MathOperator(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            /**
             * The execute() method triggers each Node recursively
             */
            public int execute() {
                return 0;
            }
        }

        /**
         * Parent Node class for Logical Operators (>, >=, ==, <=, <)
         */
        private class LogicOperator extends Node {
            public LogicOperator(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            /**
             * The execute() method triggers each Node recursively
             */
            public boolean execute() {
                return true;
            }
        }

        /**
         * Node class for Greater Than operator
         */
        private class GT extends LogicOperator {
            public GT(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            /**
             * Custom implemnetation of execute() method for GT operator
             * @return true or false
             */
            public boolean execute() {
                return getIntValue(children.get(1)) > getIntValue(children.get(0));
            }
        }

        /**
         * Node class for Greater or Equal operator
         */
        private class GE extends LogicOperator {
            public GE(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            /**
             * Custom implementation for GE operator
             * @return True or False
             */
            public boolean execute() {
                return getIntValue(children.get(1)) >= getIntValue(children.get(0));
            }
        }

        /**
         * Node class for Equality Operator
         */
        private class EQ extends LogicOperator {
            public EQ(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            /**
             * Custom implementation for equality operator
             * @return true or false
             */
            public boolean execute() {
                return getIntValue(children.get(1)) == getIntValue(children.get(0));
            }
        }

        /**
         * Node class for Less Than operator
         */
        private class LT extends LogicOperator {
            public LT(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            /**
             * Custom implemntation for LT operator
             * @return true or false
             */
            public boolean execute() {
                return getIntValue(children.get(1)) < getIntValue(children.get(0));
            }
        }

        /**
         * Node class for Lesser or Equal operator
         */
        private class LE extends LogicOperator {
            public LE(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            /**
             * Custom implementation for LE operator
             * @return true or false
             */
            public boolean execute() {
                return getIntValue(children.get(1)) <= getIntValue(children.get(0));
            }
        }

        private class ASSGN extends LogicOperator {

            public ASSGN(Token t, Queue<Runnable> q, ArrayList<String> o) { super(t,q,o); }

            public boolean execute() {
                String varname = children.get(1).getTValue();
                Token varvalue;
                if(children.get(0).token.isOperator()) {
                    int outcome = getIntValue(children.get(0));
                    varvalue = new Token("INTEGER", Integer.toString(outcome), getTNest());
                } else if(children.get(0).token.isAtom()) {
                    varvalue = children.get(0).getToken(getTNest());
                } /* else if(children.get(0).token.isPredicate()) {
                    varvalue = children.get(0).getTokenList();
                } */
                queue.add(new ActionAssign(varname, varvalue, output));
                return true;
            }

        }

        /**
         * Node class for Prolog Variable.
         */
        private class pVariable extends Node {
            public pVariable(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            /**
             * Custom implementation for Variable
             * @return the variable name
             */
            public String execute() {
                Token t = variables.get(getTValue());
                if (t == null) {
                    return "";
                } else {
                    return t.getTValue();
                }
            }
        }

        /**
         * Node class for Prolog Integer
         */
        private class pInteger extends Node {
            public pInteger(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            /**
             * Custom implemnetation for Integer
             * @return the integer value
             */
            public int execute() {
                return Integer.parseInt(getTValue());
            }
        }

        /**
         * Node class for Prolog String
         */
        private class pString extends Node {
            public pString(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            /**
             * Custom implementation for String
             * @return the string itself
             */
            public String execute() {
                return getTValue();
            }
        }

        /**
         * Node class for Plus Operator
         */
        private class Plus extends MathOperator {
            public Plus(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            /**
             * Custom implementation for the Plus Operator
             * @return the sum of integers tried from its children
             */
            public int execute() {
                return getIntValue(children.get(1)) + getIntValue(children.get(0));
            }
        }

        /**
         * Node class for Minus Operator
         */
        private class Minus extends MathOperator {
            public Minus(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            public int execute() {
                return getIntValue(children.get(1)) - getIntValue(children.get(0));
            }
        }

        /**
         * Node class for Multiplication Operator
         */
        private class Multi extends MathOperator {
            public Multi(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            /**
             * Custom implementation for Multiplication
             * @return the product of integers retrieved from it children
             */
            public int execute() {
                return getIntValue(children.get(1)) * getIntValue(children.get(0));
            }
        }

        /**
         * Node class for Division Operator
         */
        private class Div extends MathOperator {
            public Div(Token t, Queue<Runnable> q, ArrayList<String> o) {
                super(t, q, o);
            }

            /**
             * Custom implementation for  Division
             * @return the quotient
             */
            public int execute() {
                return getIntValue(children.get(1)) / getIntValue(children.get(0));
            }
        }

        /**
         * This method automates recasting so as to extract the executed value for Nodes that return integers
         */
        private int getIntValue(Node child) {
            if (child.token.isInteger()) {
                return ((pInteger) child).execute();
            } else if (child.token.isOperator()) {
                if (child.getTValue().equals("+")) {
                    return ((Plus) child).execute();
                } else if (child.getTValue().equals("-")) {
                    return ((Minus) child).execute();
                } else if (child.getTValue().equals("*")) {
                    return ((Multi) child).execute();
                } else if (child.getTValue().equals("/")) {
                    return ((Div) child).execute();
                } else { return -1; }
            } else if (child.token.isVariable()) {
                Token t = variables.get(child.getTValue());
                return Integer.parseInt(t.getTValue());
            } else { return -1; }
        }

        /**
         * This method automates recasting so as to extract the executed value for Nodes that return boolean
         */
        private boolean getBoolValue(Node child) {
            if (child.token.isLogic()) {
                if (child.getTValue().equals(">")) {
                    return ((GT) child).execute();
                } else if (child.getTValue().equals(">=")) {
                    return ((GE) child).execute();
                } else if (child.getTValue().equals("==")) {
                    return ((EQ) child).execute();
                } else if (child.getTValue().equals("<=")) {
                    return ((LE) child).execute();
                } else if (child.getTValue().equals("<")) {
                    return ((LT) child).execute();
                } else if (child.getTValue().equals("=")) {
                    return ((ASSGN) child).execute();
                } else { return false; }
            } else { return false; }
        }

        /**
         * The Generator class is the core logic engine component. It contains the
         * Abstract Syntax Tree (AST) which is built from each Prolog clause.
         */
        private class Generator implements Iterator<Boolean> {

            protected ArrayList<String> output;
            protected LinkedList<Runnable> queue = new LinkedList<Runnable>();
            private Node root;
            private boolean outcome;

            /**
             * Default constructor
             *
             * @param tokens,     an ordered ArrayList containing the Prolog tokens
             * @param ext_output, an ArrayList to store the stdout
             */
            public Generator(ArrayList<Token> tokens, ArrayList<String> ext_output) {
                output = ext_output;
                Stack<Token> mystack = toRPN(tokens);
                System.out.println("Generator: original tokens " + tokens);
                System.out.println("Generator: RPN Stack is " + mystack);
                buildAST(mystack, queue, null);
            }

            /**
             * Part of the Iterator interface to facilitate backtracking
             */
            public boolean hasNext() {
                return queue.size() > 0;
            }

            /**
             * Part of the Interator interface to facilitate backtracking
             */
            public Boolean next() {
                if (queue.size() > 0) {
                    queue.remove().run();
                    return true;
                }
                return false;
            }

            /**
             * Optional [unused] component of the Iterator interface
             */
            public void remove() {
                //do nothing
            }

            /**
             * This method executes the abstract syntax tree and add the output to
             * the action queue which is called upon during backtracking
             */
            public void execute() {
                // the execute() method adds action to the action queue
                if (root.token.isPredicate()) {
                    ((Predicate) root).execute();
                } else if (root.token.isMath()) {
                    int outcome = 0;
                    switch (root.token.getTValue()) {
                        case "+":
                            outcome = ((Plus) root).execute();
                            break;
                        case "-":
                            outcome = ((Minus) root).execute();
                            break;
                        case "*":
                            outcome = ((Multi) root).execute();
                            break;
                        case "/":
                            outcome = ((Div) root).execute();
                            break;
                    }
                    queue.add(new ActionWriteOutput(Integer.toString(outcome), this.output));
                } else if (root.token.isLogic()) {
                    boolean outcome = false;
                    switch (root.token.getTValue()) {
                        case ">":
                            outcome = ((GT) root).execute();
                            break;
                        case ">=":
                            outcome = ((GE) root).execute();
                            break;
                        case "==":
                            outcome = ((EQ) root).execute();
                            break;
                        case "<=":
                            outcome = ((LE) root).execute();
                            break;
                        case "<":
                            outcome = ((LT) root).execute();
                            break;
                    }
                    queue.add(new ActionWriteOutput(Boolean.toString(outcome), this.output));
                }
            }

            /**
             * This method automates the creation of AST Node of the correct type, then recast it to
             * the parent class. This is required because in subsequent use, the Nodes may be recasted
             * to its original class so as to access its own special methods (e.g. execute()).
             * @param t Prolog token
             * @param q Action Queue
             * @param o ArrayList for String Output
             * @return the Node
             */
            private Node createNode(Token t, Queue q, ArrayList<String> o) {
                if(t.isPredicate()) {
                    return (Node) new Predicate(t,q,o);
                } else if (t.isInteger()) {
                    return (Node) new pInteger(t,q,o);
                } else if (t.isString()) {
                    return (Node) new pString(t,q,o);
                } else if(t.isAtom()) {
                    return (Node) new pAtom(t,q,o);
                } else if(t.isMath()) {
                    switch(t.getTValue()) {
                        case "+":
                            return (Node) new Plus(t,q,o);
                        case "-":
                            return (Node) new Minus(t,q,o);
                        case "*":
                            return (Node) new Multi(t,q,o);
                        case "/":
                            return (Node) new Div(t,q,o);
                    }
                } else if(t.isLogic()) {
                    switch(t.getTValue()) {
                        case ">":
                            return (Node) new GT(t,q,o);
                        case ">=":
                            return (Node) new GE(t,q,o);
                        case "==":
                            return (Node) new EQ(t,q,o);
                        case "<=":
                            return (Node) new LE(t,q,o);
                        case "<":
                            return (Node) new LT(t,q,o);
                    }
                }
                return new Node(t,q,o);
            }

            /**
             * This method constructs the Abstrat Syntax Tree
             * @param tokens ordered ArrayList of Prolog Tokens
             * @param parent the parent node for the current Node to be added
             */
            private void buildAST(Stack<Token> tokens, Queue queue, Node parent) {
                if (tokens.size() == 0) {
                    return;
                }
                Token tCurrent = tokens.pop();
                Node child = createNode(tCurrent, queue, output);

                String p;
                if(parent==null) { p = "root"; } else { p = parent.toString(); }
                System.out.println("BuildAST: Currently processing parent " + p + " child " + child);

                if (parent == null) {
                    root = child;
                    buildAST(tokens, queue, root);
                } else if (parent.token.isOperator()) {
                    int nChildren = 2;
                    while (parent.nChildren() < nChildren) {
                        parent.addChild(child);
                        System.out.println(child + " added to " + parent);
                        if (child.token.isOperator() || child.token.isPredicate()) {
                            buildAST(tokens, queue, child);
                        }
                        if (parent.nChildren() == nChildren) {
                            break;
                        } else {
                            if (tokens.size() == 0) {
                                break;
                            }
                            tCurrent = tokens.pop();
                            child = createNode(tCurrent, queue, output);
                        }
                    }
                } else if (parent.token.isBuiltin()) {
                    // The builtins read, write and consult only have one child (parameter) each.
                    parent.addChild(child);
                    System.out.println(child + " added to " + parent);
                    buildAST(tokens, queue, parent);
                } else if (parent.token.isPredicate()) {
                    int pNest = parent.token.getTNest();
                    int cNest = child.token.getTNest();
                    if(cNest > pNest) {
                        parent.addChild(child);
                        System.out.println("child" + parent.nChildren() + " " + child + " added to " + parent);
                        buildAST(tokens,queue,parent);
                    }
                }
            }
        }



        /**
         * Default constructor for the LittleProlog Interpreter
         * @param db The Prolog Database. Default value is null.
         */
        public LittleProlog(HashMap<String, SearchTrie> db) {
            this.WEIGHT.put("=", 100);
            this.WEIGHT.put("+", 200);
            this.WEIGHT.put("-", 200);
            this.WEIGHT.put("*", 400);
            this.WEIGHT.put("/", 400);
            this.WEIGHT.put("==", 700);
            this.WEIGHT.put(">", 700);
            this.WEIGHT.put(">=", 700);
            this.WEIGHT.put("<", 700);
            this.WEIGHT.put("<=", 700);
            this.WEIGHT.put(":-", 1200);
            if(db == null) {
                database = new HashMap<String, SearchTrie>();
            } else {
                database = db;
            }
        }

        /**
         * This method resets the database
         */
        public void reset() {
            database.clear();
        }

        /**
         * This method add facts to the database. It doesn't support clauses, ie. the :- operator.
         * @param prolog
         * @return
         */
        public String onDatabase(String prolog) {
            Stack<Token> tokens = makeStack(getTokens(prolog));
            ArrayList<Token> fact = new ArrayList<Token>();
            Token current, rToken;
            if(tokens.size() == 0) {
                return "Failed to add facts.";
            }
            while(tokens.size() > 0) {
                current = tokens.pop();
                if(current.getTClass().equals("STOP")) {
                    rToken = fact.get(0);
                    if(database.get(rToken.toString()) == null) { database.put(rToken.toString(), new SearchTrie(rToken)); }
                    ((SearchTrie) database.get(rToken.toString())).insert(fact);
                    fact.clear();
                } else {
                    fact.add(current);
                }
            }
            return "Facts added to database.";
        }

        /**
         * This method processes Prolog queries
         * @param prolog, a string representation of the Prolog code
         * @return the output of the program as a String
         */
        public String onQuery(String prolog) {
            Stack<Token> stack = makeStack(getTokens(prolog));
            ArrayList<Generator> generators = new ArrayList<Generator>();
            ArrayList<String> output = new ArrayList<String>();
            Token tCurrent;
            ArrayList<Token> tokens = new ArrayList<Token>();
            while(stack.size() > 0) {
                tCurrent = stack.pop();
                if (tCurrent.isStop()) {
                    generators.add(new Generator(tokens, output));
                    tokens.clear();
                    invoke(generators, 0);
                    generators.clear();
                    variables.clear();
                } else if (tCurrent.isNext()) {
                    generators.add(new Generator(tokens, output));
                    tokens.clear();
                } else {
                    tokens.add(tCurrent);
                }

            }
            String display = "";
            for(String s: output) {
                display += s;
            }
            return display;
        }

        /**
         * This method is a wrapper for the tokeniser and the makeStack methods.
         * @param prolog a String containing the Prolog source code
         * @return  returns a stack of tokens
         */
        public ArrayList<Token> getTokens(String prolog) {
            ArrayList<Token> tokens = new ArrayList<Token>();
            try {
                tokens = tokenize(prolog, 0, 0, tokens);
            } catch(SyntaxError e) {
                tokens.clear();
            }
            return tokens;
        }

        /**
         * This method is the tokeniser.
         * @param line

         * @param i
         * @param level
         * @param tokens
         * @return
         * @throws SyntaxError
         */
        private ArrayList<Token> tokenize(String line, int i, int level, ArrayList<Token> tokens) throws SyntaxError {
            Character c;
            String tclass, tvalue;
            Boolean flagged = false;
            int j;
            int oplen = 1;
            if (i < line.length()) {
                c = line.charAt(i);
                if(c == '.') {
                    tokens.add(new Token("STOP", ".", 0));
                    return tokenize(line, i+1, level, tokens);
                } else if (c == ',') {
                    if (level == 0) {
                        tokens.add(new Token("NEXT", ",", 0));
                    }
                    return tokenize(line, i+1, level, tokens);
                } else if(substring(line, i, i+2).equals("/*")) {
                    try {
                        while( ! line.substring(i,i+2).equals("*/")) {
                            i += 1;
                        }
                        return tokenize(line, i+2, level, tokens);
                    } catch(IndexOutOfBoundsException e) {
                        throw new SyntaxError("Bad Remark Syntax");
                    }
                } else if (c == ')') {
                    return tokenize(line, i+1, level-1, tokens);
                } else if (Character.isWhitespace(c)) {
                    return tokenize(line, i+1, level, tokens);
                }
                // Determine token class
                if(Character.isDigit(c)) {
                    tclass = "INTEGER";
                } else if ( c == '_') {
                    tclass = "VARIABLE";
                } else if (Character.isLetter(c)) {
                    if (Character.isUpperCase(c)) {
                        tclass = "VARIABLE";
                    } else {
                        tclass = "ATOM";
                    }
                } else if (c == ':' && WEIGHT.containsKey(substring(line, i, i+2))) {
                    tclass = "OPERATOR";
                    oplen = 2;
                } else if (WEIGHT.containsKey(Character.toString(c))) {
                    tclass = "OPERATOR";
                    oplen = 1;
                } else if (c == '\'') {
                    tclass = "STRING";
                    i += 1;
                } else {
                    throw new SyntaxError("Unrecognised Symbol");
                }
                j = i;
                if (tclass.equals("INTEGER")) {
                    c = line.charAt(j);
                    while (Character.isDigit(c)) {
                        j += 1;
                        if (j < line.length()) {
                            c = line.charAt(j);
                        } else {
                            break;
                        }
                    }
                    tokens.add(new Token(tclass, substring(line, i,j), level));
                    return tokenize(line, j, level, tokens);
                } else if (tclass.equals("STRING")) {
                    c = line.charAt(j);
                    while (c != '\'') {
                        j += 1;
                        if (j < line.length()) {
                            c = line.charAt(j);
                        } else {
                            flagged = true;
                            break;
                        }
                    }
                    if(flagged) {
                        throw new SyntaxError("String failed to enclose.");
                    } else {
                        tokens.add(new Token(tclass, substring(line, i,j), level));
                        return tokenize(line, j+1, level, tokens);
                    }
                } else if (tclass.equals("ATOM")) {
                    c = line.charAt(j);
                    while (Character.isLetterOrDigit((c))) {
                        j += 1;
                        if(j < line.length()) {
                            c = line.charAt(j);
                        } else {
                            break;
                        }
                    }
                    j = skipWhitespace(line, j);
                    if(line.charAt(j) == '(') {
                        tclass = "PREDICATE";
                        tokens.add(new Token(tclass, substring(line, i,j),level));
                        return tokenize(line, j+1, level+1, tokens);
                    } else {
                        tokens.add(new Token(tclass, substring(line, i,j), level));
                        return tokenize(line, j, level, tokens);
                    }
                } else if (tclass.equals("VARIABLE")) {
                    c = line.charAt(j);
                    while(Character.isLetterOrDigit(c)) {
                        j += 1;
                        if(j < line.length()) {
                            c = line.charAt(j);
                        } else {
                            break;
                        }
                    }
                    tokens.add(new Token(tclass, substring(line, i,j), level));
                    return tokenize(line, j, level, tokens);
                } else if (tclass.equals("OPERATOR")) {
                    tvalue = substring(line, i, i+oplen);
                    tokens.add(new Token(tclass, tvalue, level));
                    return tokenize(line, i+oplen, level, tokens);
                } else {
                    return tokens;
                }
            }
            return tokens;
        }

        public int skipWhitespace(String line, int j) {
            while(Character.isWhitespace(line.charAt(j))) { j++; }
            return j;
        }

        /**
         * This method converts an ArrayList of tokens into a stack. The token order in the stack is the reverse of that of the ArrayList.
         * @param tokens the original ArrayList
         * @return a stack of tokens
         */
        private Stack<Token> makeStack(ArrayList<Token> tokens) {
            Stack<Token> tstack = new Stack<Token>();
            if (tokens.size() > 0) {
                int i;
                for(i=tokens.size()-1; i>= 0; i--) {
                    tstack.push(tokens.get(i));
                }
            }
            return tstack;
        }

        /**
         * This method faciliates the backchaining of each generator to automate backtracking
         * @param generators an ordered Arraylist containing Generator objects
         * @param i the index number of the current Generator object to be backchained. The initial value of i is 0.
         */
        private void invoke(ArrayList<Generator> generators, int i) {
            if(generators.size() > 0 && i < generators.size()) {
                Generator current = generators.get(i);
                current.execute();
                while(current.hasNext()) {
                    if(current.next()) {
                        invoke(generators, i+1);
                    }
                }
            }
        }

        /**
         * @tokens  the Prolog tokens of a single line of code
         * @i       the index number of the current token . Default value is 0.
         * @rpnmode whether the program is in RPN mode. Default value is True.
         */
        public Stack<Token> toRPN(ArrayList<Token> tokens) {
            boolean rpnmode = false;
            Stack<Token> newstack = new Stack<Token>();
            Stack<Token> rpnstack = new Stack<Token>();
            Stack<Token> opstack = new Stack<Token>();
            if(tokens.size() > 0) {
                rpnmode = !tokens.get(0).isPredicate();
                int i = 0;
                while(i < tokens.size()) {
                    if(rpnmode) {

                        rpnpush(tokens.get(i), rpnstack, opstack);
                    } else {
                        newstack.add(tokens.get(tokens.size() -i - 1));
                    }
                    i++;
                }
                if(rpnmode) {
                    while(opstack.size() > 0) { rpnstack.add(opstack.pop()); }
                    return rpnstack;
                }
                return newstack;
            }
            return null;
        }

        /**
         * This method reverses the order of the given stack
         * @param stack
         * @return a revesed stack
         */
        private Stack<Token> reverse(Stack<Token> stack) {
            Stack<Token> newstack = new Stack<Token>();
            while (stack.size() > 0) {
                newstack.add(stack.pop());
            }
            return newstack;
        }

        /**
         * Implementation of Shunter's Algorithm to convert Prolog stack to RPN order
         * @param token
         * @param rpnstack
         * @param opstack

         */
        private void rpnpush(Token token, Stack<Token> rpnstack, Stack<Token> opstack) {
            System.out.println("rpnpush: " + token);
            if (token.isOperator()) {
                if (higher(token, opstack)) {
                    System.out.println("rpnpush: adding " + token + " to opstack 1");
                    opstack.add(token);
                } else {
                    System.out.println("rpnpush: moving " + opstack.peek() + " from opstack to rpnstack 2");
                    rpnstack.add(opstack.pop());
                    System.out.println("rpnpush: adding " + token + " to opstack 3");
                    opstack.add(token);
                }
            } else {
                System.out.println("rpnpush: adding " + token + "to rpnstack 4");
                rpnstack.add(token);
            }

        }

        /**
         * Part of the Shunter's Algorithm. Evaluates the weight comparison of different operators
         * @param token the token to be compared against the top token of the opstack
         * @param opstack the token at the top of the opstack is being compared to
         * @return true or false
         */
        private boolean higher(Token token, Stack<Token> opstack) {
            Token t2;
            if (opstack.size() == 0) {
                System.out.println("higher: opstack is empty");
                return true;
            } else {
                System.out.println("higher: comparing " + token.getTValue() + " against " + opstack.peek().getTValue());
                return WEIGHT.get(token.getTValue()) > WEIGHT.get(opstack.peek().getTValue());
            }
        }

        /**
         * This method is a wrapper for the string.substring method to avoid IndexOutOfBoundException
         * This also makes substring() behave closer to Python's string splicing.
         * @param string1 the string where the substring is extracted from
         * @param start the start index number
         * @param end the end index number
         * @return a substring starting at index start and ends just before index end.
         */
        private String substring(String string1, int start, int end) {
            if(start < 0) { start = 0; }
            if(end > string1.length()) { end = string1.length(); }
            return string1.substring(start, end);
        }
    }
