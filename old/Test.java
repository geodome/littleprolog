
public class Test {

    public static void main(String[] args) {
        LittleProlog test = new LittleProlog(null);
        String[] queries = {"bigger(X,Y).", "bigger(Y, cat)."};
        test.onDatabase("bigger(cow, dog).");
        test.onDatabase("bigger(dog, cat).");
        for(int i = 0; i < queries.length; i++) {
            System.out.println(i);
            System.out.println(queries[i]);
            System.out.println(test.getTokens(queries[i]));
            System.out.println("output: " + test.onQuery(queries[i]));
        }
    }
}
