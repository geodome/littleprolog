import java.io.Serializable;

/**
 * Token Class to represent a Prolog Language token

 */
public class Token implements Serializable {
    private String t_class, t_value;
    private int t_nest;

    /**
     * Default constructor.
     *
     * @param t_class, token class either 'INTEGER', 'OPERATOR', 'STRING', 'VARIABLE'
     * @param t_value, describes the token. E.g. "1" for INTEGER
     * @param t_nest,  describes the nesting level of the token
     */
    public Token(String t_class, String t_value, int t_nest) {
        this.t_class = t_class;
        this.t_value = t_value;
        this.t_nest = t_nest;
    }

    /**
     * Returns to the token class
     *
     * @return token class
     */
    public String getTClass() {
        return this.t_class;
    }

    /**
     * Returns the token value
     *
     * @return token value
     */
    public String getTValue() {
        return this.t_value;
    }

    /**
     * returns the nesting level
     *
     * @return the nesting level
     */
    public int getTNest() {
        return this.t_nest;
    }

    /**
     * Checks if the other token is the same token
     *
     * @param other another token
     * @return true or false
     */
    public boolean equals(Token other) {
        return getTClass().equals(other.getTClass()) && getTValue().equals(other.getTValue()) && getTNest() == other.getTNest();
    }

    /**
     * Extracts a string representation of the token
     *
     * @return a string representation of the token
     */
    public String toString() {
        return t_class + ", " + t_value + ", " + t_nest;
    }

    /**
     * Checks if the token is an Operator
     *
     * @return True or False
     */
    public boolean isOperator() {
        return t_class.equals("OPERATOR");
    }

    /**
     * Checks if the token is an integer
     *
     * @return True or False
     */
    public boolean isInteger() {
        return t_class.equals("INTEGER");
    }

    /**
     * Checks if the token is a variable
     *
     * @return True or False
     */
    public boolean isVariable() {
        return t_class.equals("VARIABLE");
    }

    /**
     * check if token is a Predicate
     * @return True or False
     */
    public boolean isPredicate() {
        return t_class.equals("PREDICATE");
    }

    /**
     * check if token is an Atom
     * @return true or false
     */
    public boolean isAtom() { return t_class.equals("ATOM"); }

    /**
     * Check if token is a String
     * @return true or false
     */
    public boolean isString() {
        return t_class.equals("STRING");
    }

    /**
     * Check if a token is a Math operator (+,-,*,/)
     * @return true or false
     */
    public boolean isMath() {
        if (isOperator()) {
            return getTValue().equals("+") || getTValue().equals("-") || getTValue().equals("/") || getTValue().equals("*");
        } else {
            return false;
        }
    }

    /**
     * Check if a token is a Logical Operator (>, >=, ==, <=, <)
     * @return true or false
     */
    public boolean isLogic() {
        if (isOperator()) {
            return getTValue().equals(">") || getTValue().equals(">=") || getTValue().equals("==") || getTValue().equals("<=") || getTValue().equals("<");
        } else {
            return false;
        }
    }

    /**
     * Check if a topken is STOP
     */
     public boolean isStop() {
         return getTClass().equals("STOP");
     }

     /**
      * Check if the token is a NEXT
      */
     public boolean isNext() {
         return getTClass().equals("NEXT");
     }


     public boolean isClause() {
         return getTValue().equals(":-");
     }

     public boolean isBuiltin() {
         return isPredicate() && (getTValue().equals("read") || getTValue().equals("write") || getTValue().equals("consult"));
     }

     public Token getToken(int nest) {
         return new Token(t_class, t_value, nest);
     }

}
