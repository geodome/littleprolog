
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class is implementation of the Trie Node for the Search Trie
 * Its requirements are very diffferent from that of the AST Node.
 */
public class TrieNode implements Serializable {
    private HashMap<String, TrieNode> children = new HashMap<String, TrieNode>();
    private Boolean terminal = false;
    public Token token;

    /**
     * Default constructor
     * @param t the Token which the TrieNode represents
     */
    public TrieNode(Token t) { token = t; }

    /**
     * This method retrings a String represetation of the Trie Node
     * @return the string representation of the underlying token
     */
    public String toString() { return token.toString(); }

    /**
     * During insertion of a fact, a Trie Node is set as Terminal if the last token
     * coincides with the current Trie Node
     */
    public void setTerminal() { terminal = true; }

    /**
     * This method tests is a Trie Node is a Terminal
     * @return true or false
     */
    public boolean isTerminal() { return terminal; }

    /**
     * This method tests if the token in the Trie Node matches with a test token
     * @param t the test token
     * @return true or false
     */
    public boolean match(Token t) { return token.equals(t); }

    /**
     * This method is for adding a child Trie Node to the currrent Node
     * @param t the Token for the new Trie Node
     */
    public void addChild(Token t) { children.put(t.toString(), new TrieNode(t)); }

    /**
     * This method checks if the Trie Node contains a child corresponding to the given Token
     * @param t the token
     * @return true or false
     */
    public boolean hasChild(Token t) { return children.containsKey(t.toString()); }

    /**
     * This method retrieves a child Trie Node which correponds to the given token
     * To retrieve a child for token t, always test if the Trie Node has the child first
     * using the HasChild() method, before retrieving the Child Trie Node. Failure to do
     * so will lead to Null Pointe Exception should the child doesn't exist.
     * @param t
     * @return
     */
    public TrieNode getChild(Token t) { return children.get(t.toString()); }

    /**
     * This method returns all the child nodes as an ArrayList
     * @return an ArrayList of child Trie Nodes
     */
    public ArrayList<TrieNode> getChildren() { return new ArrayList<TrieNode>(children.values()); }
}
