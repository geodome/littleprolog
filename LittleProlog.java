package bin.littleprolog;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Stack;
import bin.littleprolog.database.SearchTrie;
import bin.littleprolog.ast.Generator;
import bin.littleprolog.common.*;


public class LittleProlog {

    private HashMap<String, SearchTrie> database;
    private HashMap<String, Token> variables;
    private ArrayList<Generator> generators;
    private Tokeniser tk;

    public LittleProlog() {
        database = new HashMap<String, SearchTrie>();
        variables = new HashMap<String, Token>();
        generators = new ArrayList<Generator>();
        tk = new Tokeniser();
    }

    public void reset() { database.clear(); variables.clear(); }

    public void onDatabase(String prolog) {
        Stack<Token> tokens = tk.toRPN(tk.process(prolog));
        ArrayList<Token> fact = new ArrayList<Token>();
        Token current, rToken;
        if(tokens.size() == 0) {
            System.out.println("Failed to add facts.");
        }
        while(tokens.size() > 0) {
            current = tokens.pop();
            if(current.isStop()) {
                rToken = fact.get(0);
                if(database.get(rToken.toString()) == null) { database.put(rToken.toString(), new SearchTrie(rToken)); }
                ((SearchTrie) database.get(rToken.toString())).insert(fact);
                fact.clear();
            } else {
                fact.add(current);
            }
        }
        System.out.println("Facts added to database.");
    }

    public void onQuery(String prolog) {
        int i, start, end;
        i = 0; start = 0; end = 0;
        Token tCurrent;
        Stack<Token> sCurrent;
        ArrayList<Token> tokens = tk.process(prolog);
        while(i < tokens.size()) {
            tCurrent = tokens.get(i);
            if(tCurrent.isNext() || tCurrent.isStop()) {
                end = i;
                sCurrent = tk.toRPN(tokens.subList(start, end));
                generators.add(new Generator(sCurrent, database, variables, generators.size()));
                start = i + 1;
            }
            if (tCurrent.isStop()) {
                invoke(0);
                generators.clear();
                variables.clear();
            }
            i += 1;
        }
    }

    private void invoke(int i) {
        if(generators.size() > 0 && i < generators.size()) {
            Generator current = generators.get(i);
            current.execute();
            while(current.hasNext()) {
                if(current.next()) {
                    invoke(i+1);
                }
            }
        }
    }

}
