package bin.littleprolog.ast;

import java.util.Iterator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Stack;
import bin.littleprolog.database.SearchTrie;
import bin.littleprolog.common.Token;

public class Generator implements Iterator<Boolean> {

    private LinkedList<Runnable> queue;
    private int level;
    private AST ast;
    private HashMap<String, SearchTrie> database;
    private HashMap<String, Token> variables;

    /**
     * Default constructor
     *
     * @param tokens,     an ordered ArrayList containing the Prolog tokens
     * @param ext_output, an ArrayList to store the stdout
     */
    public Generator(Stack<Token> tokens, HashMap<String, SearchTrie> d, HashMap<String, Token> v, int l) {
        level = l;
        database = d;
        variables = v;
        queue = new LinkedList<Runnable>();
        ast = new AST(tokens, database, variables, queue, level);
    }

    /**
     * Part of the Iterator interface to facilitate backtracking
     */
    public boolean hasNext() {
        return queue.size() > 0;
    }

    /**
     * Part of the Interator interface to facilitate backtracking
     */
    public Boolean next() {
        if (queue.size() > 0) {
            queue.remove().run();
            return true;
        }
        return false;
    }

    /**
     * Optional [unused] component of the Iterator interface
     */
    public void remove() {
        //do nothing
    }

    public void execute() {
        ast.execute();
    }

}
