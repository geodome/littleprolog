package bin.littleprolog.ast;

import java.lang.Integer;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Stack;
import bin.littleprolog.common.Token;
import bin.littleprolog.common.NParam;
import bin.littleprolog.common.SyntaxError;
import bin.littleprolog.database.*;

public class AST {

    private HashMap<String, SearchTrie> database;
    private HashMap<String, Token> variables;
    private Queue<Runnable> queue;
    private int level;
    private Node root;

    public AST(Stack<Token> tokens, HashMap<String, SearchTrie> d, HashMap<String, Token> v, Queue<Runnable> q, int l) {
        database = d;
        variables = v;
        queue = q;
        level = l;
        buildAST(tokens, null);
    }

    private void buildAST(Stack<Token> tokens, Node parent) {
        if (tokens.size() == 0) {
            return;
        }
        if (parent == null) {
            root = createNode(tokens.pop());
            buildAST(tokens, root);
        } else if (parent.token.isOperator()) {
            int nChildren = 2;
            for(int i=0; i<nChildren; i++) {
                Node child = createNode(tokens.pop());
                parent.addChild(child);
                if(child.token.isOperator()) {
                    buildAST(tokens, child);
                }
            }
        } else if (parent.token.isPredicate()) {
            Token tParam = tokens.pop();
            NParam nparam = (NParam) tParam;
            parent.addChild(createNode(tParam));
            for(int i=1; i<= nparam.getCount(); i++) {
                Node child = createNode(tokens.pop());
                parent.addChild(child);
                if(child.token.isPredicate()) {
                    buildAST(tokens, child);
                }
            }
        }
    }

    /**
     * This method executes the abstract syntax tree and add the output to
     // the execute() method adds action to the action queue
     * the action queue which is called upon during backtracking
     */
    public void execute() {
        if(root == null) return;
        if (root.token.isPredicate()) {
            ((Predicate) root).execute();
        } else if (root.token.isMath()) {
            int outcome = 0;
            switch (root.token.getTValue()) {
                case "+":
                    outcome = ((Plus) root).execute();
                    break;
                case "-":
                    outcome = ((Minus) root).execute();
                    break;
                case "*":
                    outcome = ((Multi) root).execute();
                    break;
                case "/":
                    outcome = ((Div) root).execute();
                    break;
            }
            queue.add(new ActionWriteOutput(Integer.toString(outcome), level));
        } else if (root.token.isLogic()) {
            boolean outcome = false;
            switch (root.token.getTValue()) {
                case ">":
                    outcome = ((GT) root).execute();
                    break;
                case ">=":
                    outcome = ((GE) root).execute();
                    break;
                case "==":
                    outcome = ((EQ) root).execute();
                    break;
                case "<=":
                    outcome = ((LE) root).execute();
                    break;
                case "<":
                    outcome = ((LT) root).execute();
                    break;
                case "=":
                    outcome = ((ASSGN) root).execute();
                    break;
            }
            queue.add(new ActionWriteOutput(Boolean.toString(outcome), level));
        }
    }

    public int getIntValue(Node child) {
        if (child.token.isInteger()) {
            return ((pInteger) child).execute();
        } else if (child.token.isOperator()) {
            if (child.token.getTValue().equals("+")) {
                return ((Plus) child).execute();
            } else if (child.token.getTValue().equals("-")) {
                return ((Minus) child).execute();
            } else if (child.token.getTValue().equals("*")) {
                return ((Multi) child).execute();
            } else if (child.token.getTValue().equals("/")) {
                return ((Div) child).execute();
            } else { return -1; }
        } else if (child.token.isVariable()) {
            Token t = variables.get(child.token.getTValue());
            return Integer.parseInt(t.getTValue());
        } else { return -1; }
    }

    /**
     * This method automates recasting so as to extract the executed value for Nodes that return boolean
     */
    public boolean getBoolValue(Node child) {
        if (child.token.isLogic()) {
            if (child.token.getTValue().equals(">")) {
                return ((GT) child).execute();
            } else if (child.token.getTValue().equals(">=")) {
                return ((GE) child).execute();
            } else if (child.token.getTValue().equals("==")) {
                return ((EQ) child).execute();
            } else if (child.token.getTValue().equals("<=")) {
                return ((LE) child).execute();
            } else if (child.token.getTValue().equals("<")) {
                return ((LT) child).execute();
            } else if (child.token.getTValue().equals("=")) {
                return ((ASSGN) child).execute();
            } else { return false; }
        } else { return false; }
    }

    public Node createNode(Token t) {
        if(t.isPredicate()) {
            return (Node) new Predicate(t);
        } else if (t.isInteger()) {
            return (Node) new pInteger(t);
        } else if (t.isString()) {
            return (Node) new pString(t);
        } else if(t.isAtom()) {
            return (Node) new Atom(t);
        } else if(t.isMath()) {
            switch(t.getTValue()) {
                case "+":
                    return (Node) new Plus(t);
                case "-":
                    return (Node) new Minus(t);
                case "*":
                    return (Node) new Multi(t);
                case "/":
                    return (Node) new Div(t);
            }
        } else if(t.isLogic()) {
            switch(t.getTValue()) {
                case ">":
                    return (Node) new GT(t);
                case ">=":
                    return (Node) new GE(t);
                case "==":
                    return (Node) new EQ(t);
                case "<=":
                    return (Node) new LE(t);
                case "<":
                    return (Node) new LT(t);
                case "=":
                    return (Node) new ASSGN(t);
            }
        }
        return new Node(t);
    }


    public class Node {

        public Token token;
        public ArrayList<Node> children = new ArrayList<Node>();

        /**
         * Default constructor
         * @param t Prolog token
         */
        public Node(Token t) {
            token = t;
        }

        public String toString() {
            return token.toString();
        }

        public Token getToken() {
            return token;
        }

        public void addChild(Node child) {
            children.add(child);
        }

        /**
         * This method informs the number of children a node has
         */
        public int nChildren() {
            return children.size();
        }

        /**
         * This method retrieves a child node
         */
        public Node getChild(int i) {
            return children.get(i);
        }


        /**
         * This method generates the Token List
         */
        public ArrayList<Token> getTokenList() {
            ArrayList<Token> aggregate = new ArrayList<Token>();
            if(!token.isNParam()) {
                aggregate.add(token);
                for(int i=0; i < children.size(); i++) {
                    ArrayList<Token> tokens = getChild(i).getTokenList();
                    aggregate.addAll(tokens);
                }
            }
            return aggregate;
        }

        // public void execute() {}
    }

    public class Predicate extends Node {

        public Predicate(Token t) {
            super(t);
        }

        public void execute() {
            try {
                switch(token.getTValue()) {
                    case "read":
                        read();
                        break;
                    case "write":
                        write();
                        break;
                    case "consult":
                        consult();
                        break;
                    case "exit":
                        exit();
                        break;
                    default:
                        search();
                }
            } catch (SyntaxError e) {
                System.out.println(e);
            }
        }

        // add read and consult builtin later
        public void read() {}
        public void consult() {}

        public void exit() {
            System.exit(0);
        }

        public void write() throws SyntaxError {
            int n = ((NParam) children.get(0).token).getCount();
            if(n == 1) {
                String o;
                Node p = children.get(1);
                if(p.token.isString() || p.token.isInteger() || p.token.isAtom()) {
                    o = p.token.getTValue();
                } else if (p.token.isVariable()){
                    String varname = p.token.getTValue();
                    if(variables.containsKey(varname)) {
                        Token t = variables.get(varname);
                        o = t.getTValue();
                    } else {
                        o = "Variable " + varname + " doesn't exist.";
                    }
                } else {
                    throw new SyntaxError("The write builtin doesn't recognise the parameter.");
                }
                queue.add(new ActionWriteOutput(o, level));
            } else {
                throw new SyntaxError("The write builtin requires one parameter.");
            }
        }


        public void search() {
            SearchTrie trie = (SearchTrie) database.get(token.toString());
            if (trie == null) {
                queue.add(new ActionWriteOutput("No such fact", level));
            } else {
                Tracker result;
                ArrayList<Token> searchtokens = getTokenList();
                for (result = trie.search(searchtokens, variables); result.hasNext(); ) {
                    ArrayList<Tuple<Token>> resolutions = result.next();
                    queue.add(new ActionSetVariable(resolutions, level, variables));
                }
                if (result.noMatches()) {
                    queue.add(new ActionWriteOutput("No matches found", level));
                }

            }
        }

    }

    public class pInteger extends Node {

        public pInteger(Token t) { super(t); }

        public int execute() { return Integer.parseInt(token.getTValue()); }
    }

    public class pString extends Node {

        public pString(Token t) { super(t); }

        public String execute() { return token.getTValue(); }
    }

    public class Variable extends Node {

        public Variable(Token t) { super(t); }

        public String execute() {
            String varname = token.getTValue();
            Token t = variables.get(varname);
            if(t == null) {
                return "";
            } else {
                return t.getTValue();
            }
        }

    }

    public class Atom extends Node {

        public Atom(Token t) {
            super(t);
        }

        public String execute() {
            return token.getTValue();
        }

    }

    public class Plus extends Node {

        public Plus(Token t) {
            super(t);
        }

        public int execute() {
            return getIntValue(children.get(1)) + getIntValue(children.get(0));
        }
    }

    public class Minus extends Node {

        public Minus(Token t) {
            super(t);
        }

        public int execute() {
            return getIntValue(children.get(1)) - getIntValue(children.get(0));
        }
    }

    public class Multi extends Node {

        public Multi(Token t) {
            super(t);
        }

        public int execute() {
            return getIntValue(children.get(1)) * getIntValue(children.get(0));
        }
    }

    public class Div extends Node {

        public Div(Token t) {
            super(t);
        }

        public int execute() {
            return getIntValue(children.get(1)) / getIntValue(children.get(0));
        }
    }

    public class GT extends Node {

        public GT(Token t) { super(t); }

        public boolean execute() {
            return getIntValue(children.get(1)) > getIntValue(children.get(0));
        }
    }

    public class GE extends Node {

        public GE(Token t) { super(t); }

        public boolean execute() {
            return getIntValue(children.get(1)) >= getIntValue(children.get(0));
        }
    }

    public class EQ extends Node {

        public EQ(Token t) { super(t); }

        public boolean execute() {
            return getIntValue(children.get(1)) == getIntValue(children.get(0));
        }
    }

    public class LE extends Node {

        public LE(Token t) { super(t); }

        public boolean execute() {
            return getIntValue(children.get(1)) <= getIntValue(children.get(0));
        }
    }

    public class LT extends Node {

        public LT(Token t) { super(t); }

        public boolean execute() {
            return getIntValue(children.get(1)) < getIntValue(children.get(0));
        }
    }

    public class ASSGN extends Node {

        public ASSGN(Token t) { super(t); }

        public boolean execute() {

            return true;
        }
    }


}
