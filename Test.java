package bin.littleprolog.tests;

import bin.littleprolog.*;

public class Test {

    public static void main(String[] args) {
        LittleProlog test = new LittleProlog();
        String prolog1 = "1 +2+ 3, hello(world1, world2).";
        test.onQuery(prolog1);
    }
}
