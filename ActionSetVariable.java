package bin.littleprolog.ast;

import java.lang.Runnable;
import java.util.ArrayList;
import java.util.HashMap;
import bin.littleprolog.common.Token;
import bin.littleprolog.database.Tuple;

public class ActionSetVariable implements Runnable {
    ArrayList<Tuple<Token>> resolutions;
    HashMap<String, Token> variables;
    String prefix = "|---";

     /**
      * Default constructor
      * @param r an ArrayList containing tuples which matches a variable to a Token (atom, string, integer)
      * @param o an ArrayList to store the output string
      */
     public ActionSetVariable(ArrayList<Tuple<Token>> r, int l, HashMap<String, Token> v) {
         resolutions = r;
         variables = v;
         for(int i=0; i<l; i++) {
             prefix = "   " + prefix;
         }
     }

     public void run() {
         for (Tuple<Token> r : resolutions) {
             if(r.isEmpty()) {
                 // Tracker adds an empty tuple to resolutions for a successful predicate search
                 System.out.println("Predicate found\n");
                 break;
             }
             Token var = r.getLeft();
             Token val = r.getRight();
             variables.put(var.getTValue(), val);
             System.out.println(prefix + " " + var.getTValue() + " = " + val.getTValue());
         }
     }
}
